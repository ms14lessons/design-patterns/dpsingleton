public class SingletonObject {
    private static SingletonObject singletonObject;
    private static int count = 0;
    private SingletonObject() {
        System.out.println("Object Created");
    }
    public static SingletonObject getSingletonObject() {
        if (singletonObject == null) {
            synchronized (SingletonObject.class) {
                if (singletonObject == null) {
                    singletonObject = new SingletonObject();
                }
            }
        }
        count++;
        System.out.println(count);
        return singletonObject;
    }
}
